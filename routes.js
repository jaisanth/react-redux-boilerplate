import React, { Component } from 'react';
import { Route, IndexRoute } from 'react-router';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import ListPage from './components/ListPage/ListPage.js';
import ComicDetailPage from './components/DetailPage/DetailPage.js';

class App extends Component {
    render() {
        return <div>
            {React.cloneElement(this.props.children, {
                key: this.props.location.pathname
            })}

    </div>;
    }
}

let routes = (
    <Route path="/" name="root" component={App}>
        <IndexRoute name="list" component={ListPage} />
        <Route path="/comic/:comicId" name="comic" headingText="Blah" component={ComicDetailPage} />
    </Route>
);

export default routes;