import * as actions from './actions.js';
import BaseAjaxConfig from './BaseAjaxConfig.js';

import comics from '../config/feeds.js';

let { headers } = BaseAjaxConfig;

export function fetchDataRequest() {
    return {
        type: actions.GET_XKCD_REQUEST
    }
}

export function fetchDataRequestSuccess(data) {
    return {
        type: actions.GET_XKCD_REQUEST_SUCCESS,
        data
    }
}

export function fetchDataRequestError(error) {
    return {
        type: actions.GET_XKCD_REQUEST_ERROR,
        error
    }
}

export function fetchData(comicName) {

    const comicSource = comics[comicName] || comics['xkcd'];

    return (dispatch) => {
        dispatch(fetchDataRequest());

        let query = encodeURIComponent(`select * from rss where url="${comicSource}"`);

        return fetch(BaseAjaxConfig.host + BaseAjaxConfig.prefix +  '?q=' + query + '&format=json&env=' +  encodeURIComponent('store://datatables.org/alltableswithkeys'), {
            method: "GET",
            headers
        })
        .then(response => response.ok ? response.json() : Promise.reject(new Error('Api Error. No response.')))
        .then(json => {
            dispatch(fetchDataRequestSuccess(json));
        }, err => console.log(err));
    };
}