let ua = typeof navigator !== 'undefined' ? navigator.userAgent : 'StandardUA';

let baseConfig = {
    headers: {
        'Accept': 'application/json'
    }
};

Object.assign(baseConfig, {
    host: "https://query.yahooapis.com",
    prefix: "/v1/public/yql"
});

export default baseConfig;
