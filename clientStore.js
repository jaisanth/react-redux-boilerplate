import thunkMiddleWare from 'redux-thunk';
import createLogger from 'redux-logger';

import { createStore, applyMiddleware } from 'redux';

import rootReducer from './reducers';

const loggerMiddleware = createLogger({collapsed: true});

let middlewares =  [thunkMiddleWare];

__DEV__ && middlewares.push(loggerMiddleware);


let initialState = window.__BUSINESS_INITIAL_STATE__;

let store = null;

if (initialState) {
    store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(...middlewares)
    );
}
else {
    store = createStore(
        rootReducer,
        applyMiddleware(...middlewares)
    );
}

export default store;