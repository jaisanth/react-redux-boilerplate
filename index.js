import React from 'react';
import { render } from 'react-dom'
import { Router, browserHistory } from 'react-router';

import { Provider } from 'react-redux';

import Routes from './routes.js';

import clientStore from './clientStore.js';

const appName = 'app-cont';


window.addEventListener('DOMContentLoaded', () => {
    let container = document.getElementById(appName);

    if (!container) {
        container = document.createElement('div');
        container.id = appName;
        container.className = appName;
        document.body.appendChild(container);
    }

    render(
        <Provider store={clientStore}>
            <Router history={browserHistory}>{Routes}</Router>
        </Provider>, container
    );
});



if(!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
        position = position || 0;
        return (this.substr(position, searchString.length) === searchString);
    };
}