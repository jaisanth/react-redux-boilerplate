import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import { fetchData } from '../../actions/actionCreators.js';

import styles from './DetailPage.css';

class DetailPage extends Component {
    constructor(args) {
        super(args);

        // State management which is internal to component only, use only for Component view state only
        this.state = {
            "hideStatus": false
        }
    }

    componentDidMount() {
        this.props.dispatch(fetchData('xkcd'));
    }

    getImageSource(imgTag) {
        let regex = /<img.*?src='(.*?)'/;

        return regex.exec(imgTag)[1];
    }



    render() {

        let { xkcd } = this.props; // Access data like this

        let { dispatch } = this.props;
        // If you are not passing {...this.props}, make sure you pass dispatch as a prop
        // to dispatch an action from child component

        let comicId = this.props.routeParams.comicId;

        console.log(xkcd.data[comicId]);

        let comicDetails = xkcd.data[comicId];


        return (
            <div className={styles['detail-page']}>
                {comicDetails ?
                    <div className={styles['detail']} >
                        <h1 className={styles['title']} >
                            {comicDetails.title}
                        </h1>
                        <div className={styles['comic']} dangerouslySetInnerHTML={{ __html:  comicDetails.description  }}>
                        </div>
                    </div>
                    :
                    null
                }

            </div>
        );
    }
}

// We map state variables as props to the component, so that it can be passed along to child components as props
function mapStateToProps(state) {
    return {
        xkcd: state.xkcdData
    };
}

export default connect(mapStateToProps)(DetailPage);