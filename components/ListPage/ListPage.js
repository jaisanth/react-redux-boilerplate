import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';

import { fetchData } from '../../actions/actionCreators.js';

import styles from './ListPage.css';

class ListPage extends Component {
    constructor(args) {
        super(args);

        // State management which is internal to component only, use only for Component view state only
        this.state = {
            "hideStatus": false
        }
    }

    componentDidMount() {
        this.props.dispatch(fetchData('xkcd'));
    }

    showDetail(index) {
        browserHistory.push(`/comic/${index}`);
    }

    render() {

        let { xkcd } = this.props; // Access data like this

        return (
            <div className={styles['home-page']}>
                <h1 className={styles['title']}>
                    Latest Comics
                </h1>

                {xkcd.data.isFetching ?
                    <div className={styles['loading']} >
                        Fetching comic list...
                    </div>
                    :
                    <ul className={styles['item-list']}>
                        {xkcd && xkcd.data && xkcd.data.map((comic, idx) => {
                            return <li key={idx} className={styles['item']} onClick={this.showDetail.bind(this, idx)} >
                                <Link to={`/comic/${idx}`}>
                                    {comic.title}
                                </Link>
                            </li>;
                        })}
                    </ul>
                }
            </div>
        );
    }
}

// We map state variables as props to the component, so that it can be passed along to child components as props
function mapStateToProps(state) {
    return {
        xkcd: state.xkcdData
    };
}

export default connect(mapStateToProps)(ListPage);