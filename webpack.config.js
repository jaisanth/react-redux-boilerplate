var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var Extract = require('extract-text-webpack-plugin');
var AssetsPlugin = require('assets-webpack-plugin');
var CompressionPlugin = require('compression-webpack-plugin');
var webpack = require('webpack');

var PROD = process.env.NODE_ENV === 'production';
var DEV = !PROD;

var plugins = PROD ? [new webpack.optimize.UglifyJsPlugin({
    compress: { warnings: false }
}), new CompressionPlugin({
        asset: "[path][query]",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8
}), new AssetsPlugin({path: path.join(__dirname, 'build'), filename: 'assets.json', fullPath: false})] : [];

module.exports = {
    entry: {
        "app": "./index.js",
        "vendor": ["react", "react-router", "react-dom", "redux", "react-redux", "redux-logger", "redux-thunk"]
    },
    output: {
        path: path.join(__dirname, 'build'),
        filename: DEV ? 'js/app.bundle.js' : 'js/app.[chunkhash:6].js'
    },
    __prod: PROD,
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: DEV ? Extract.extract('style', 'css?localIdentName=[name]_[local]_[hash:base64:6]&modules!postcss') : Extract.extract('style', 'css?localIdentName=[hash:base64:6]&modules!postcss')
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015'],
                    plugins: ['transform-object-assign', "transform-class-properties"]
                }
            },
            {
                test: /\.svg$/,
                loader: 'url?limit=3000&name=[name]_[hash:6].[ext]'
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                loader: 'url?limit=1000&name=[name]_[hash:6].[ext]'
            },
            {
                test: /\.hbs$/,
                loader: "handlebars"
            }
        ]
    },
    postcss: [
        require('precss'),
        require('postcss-cssnext')({
            browsers: ['Chrome >= 31', 'Firefox >= 31', 'IE >= 9'],
            url: false
        }),
        require('postcss-nested'),
        require('postcss-animation'),
        require('lost')
    ],
    plugins: plugins.concat([
        new webpack.ProvidePlugin({
            'Promise': 'imports?this=>global!exports?global.Promise!es6-promise',
            'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        }),
        new webpack.optimize.CommonsChunkPlugin("vendor", DEV ? "js/vendor.bundle.js" : "js/vendor.[chunkhash:6].js"),
        new webpack.DefinePlugin({
            __DEV__: DEV,
            __PROD__: PROD,
        }),
        new Extract(DEV ? 'css/bundle.css' : 'css/bundle.[contenthash:6].css', { allChunks: true })
    ])
};
