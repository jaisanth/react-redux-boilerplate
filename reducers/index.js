import { combineReducers } from 'redux';

import xkcdDataReducer from './xkcdDataReducer.js';


const rootReducer = combineReducers({
    xkcdData: xkcdDataReducer
});

export default rootReducer;