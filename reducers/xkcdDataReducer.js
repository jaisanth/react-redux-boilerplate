import {
    GET_XKCD_REQUEST,
    GET_XKCD_REQUEST_SUCCESS,
    GET_XKCD_REQUEST_ERROR
} from '../actions/actions.js';


const  parseXkcdData = (data) => {
    return data.query && data.query.results && data.query.results.item;
}

export default function xkcdData(state = {
    "isFetching": false,
    "data": []
}, action = null) {
    switch (action.type) {
        case GET_XKCD_REQUEST:
            return Object.assign({}, state, {
                "isFetching": true
            });
        case GET_XKCD_REQUEST_SUCCESS:
            return Object.assign({}, state, {
                "isFetching": false,
                "data": parseXkcdData(action.data)
            });
        default:
            return state;
    }
}