const express = require('express');
const fs = require('fs');
const path = require('path');
const compression = require('compression');

const app = express();

const PORT = process.env.PORT || 8080;

var enc = {
    encoding: 'utf-8'
};

app.use(compression());

app.use(express.static('build'));

app.get('/*', function(req, res) {
    res.header('Content-type', 'text/html');
    res.end(fs.readFileSync(path.join(__dirname, 'index.html'), enc));
});

app.listen(PORT, function() {
    console.log("Server listening on port " + PORT);
});